VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
BUILD_PLATFORMS ?= -osarch 'linux/amd64' \
					-osarch 'darwin/amd64'

PKG := $(shell go list .)

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -s -w

CI_REGISTRY ?= registry.gitlab.com
CI_REGISTRY_NAMESPACE ?= gitlab-org
CI_IMAGE_VERSION ?= ${VERSION}
CI_IMAGE ?= $(CI_REGISTRY)/$(CI_REGISTRY_NAMESPACE)/podgc/podgc:$(CI_IMAGE_VERSION)

.PHONY: compile
compile: GOOS ?= ""
compile: GOARCH ?= ""
compile:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build \
			-o build/podgc \
			-ldflags "$(GO_LDFLAGS)" .

.PHONY: test
test: GOFLAGS ?= ""
test:
	@go test -short ./... -v $(GOFLAGS)

.PHONY: test_race
test_race:
	@GOFLAGS=-race $(MAKE) test

.PHONY: test_integration
test_integration: GOFLAGS ?= ""
test_integration:
	@go test gitlab.com/gitlab-org/podgc/app -run TestCleanOnCluster -v $(GOFLAGS)

.PHONY: lint
lint: TOOL_VERSION ?= 1.27.0
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@if command -v golangci-lint > /dev/null; then \
		if ! $$(golangci-lint --version | grep "version $(TOOL_VERSION)" >/dev/null); then \
		    echo "WARNING: The installed version of golangci-lint is not the recommended version $(TOOL_VERSION)!"; \
		    golangci-lint --version; \
		fi; \
		golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS); \
	else \
	    echo "golangci-lint does not seem to be installed. Please install it at https://github.com/golangci/golangci-lint#install"; \
	fi

.PHONY: build_ci_image
build_ci_image: GO_VERSION ?= 1.13.8
build_ci_image: GO_SHASUM ?= 0567734d558aef19112f2b2873caa0c600f1b4a5827930eb5a7f35235219e9d8
build_ci_image: UBUNTU_VERSION ?= 20.04
build_ci_image: KUBECTL_VERSION ?= 1.18.0
build_ci_image:
	# Building the $(CI_IMAGE) image
	@docker build \
		--pull \
		--no-cache \
		--build-arg GO_VERSION=$(GO_VERSION) \
		--build-arg GO_SHASUM=$(GO_SHASUM) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" \
		-t $(CI_IMAGE) \
		-f dockerfiles/ci/Dockerfile \
		dockerfiles/ci/
