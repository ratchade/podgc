# PodGC

- Title: Kubernetes Pod Garbage Collation
- Team: ~"group::runner"
- Issues reference:
  - <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27870>
  - <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27702>

## Introduction

### Overview

Delete Kubernetes Pods that are no longer used to stop using resources, and only
have Pods that are necessary.

### Goal

- Least privileged should only have list/delete on the `pods` resource.
- Compliant with OpenShift.
- Running rootless container.
- Concurrent safe.
- Easy to delay.
- Audit log of what was deleted and why.
- The user decides which pods are deleted.
- CLI tool that can run inside a Cluster or users laptop.
- Support supported versions of Kubernetes.

### Non-Goals

- Automatically deciding if a pod is expired or not, the user decides this.

## Solution

### Overview

`PodGC` is an application that aims to clean the leftover pods created by executing a job with GitLab Runner Kubernetes executor.

`PodGC` is open-source and written in Go. It can be run as a single binary; no language-specific requirements are needed.
You can install GitLab Runner on several different supported operating systems.

### Execution diagramm

All the Pods that want to get garbage collected need to have `podgc.gitlab.com/ttl` Pod Annotation (configurable).
The value is an integer representing in seconds how long the Pod should be alive for.
For example `podgc.gitlab.com/ttl: 3600` means that the Pod can be deleted after it's been created for 1 hour.

When `podgc` finds a pod that is expired, it sends a [delete Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#delete-delete-a-pod) request, when multiple expired pods are found it will send the delete request sequentially for the initial iteration.

```mermaid
sequenceDiagram
    podgc->>+kubernetes: GET /api/v1/pods
    kubernetes->>-podgc: list objects of kind Pod
    Note right of podgc: Calculate expired pods
    podgc->>+kubernetes: DELETE /api/v1/namespaces/{namespace}/pods/{name}
```

### Application configuration

The application is configured through environment variables. The available variables are listed below:

| Setting                          | Description                                                                                                                                                              |
|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|`PODGC_LOG_LEVEL`                 | Log level for the PodGC application. Default: `info`                                                                                                                     |
|`PODGC_LOG_FORMAT`                | Format used for logging. Accepted values: `text` and `json`. Default: `json`                                                                                             |
|`PODGC_LIMIT`                     | Maximum number of Pods to be deleted per each `PODGC_INTERVAL` tick. Default: `15`                                                                                       |
|`PODGC_MAX_ERR_ALLOWED`           | The maximum number of errors allowed when deleting a pod. Once this limit is reached, `podgc` adds the pod in a blacklist and skips it if encountered again. Default: `5`|
|`PODGC_INTERVAL`                  | Deletion interval. It is an unsigned sequence of decimal numbers, each with optional fraction and a unit suffix, such as `300ms`, `1.5h` or `2h45m`. Valid time units are `ns`, `us` (or `µs`), `ms`, `s`, `m`, `h`. Minimum: `1s`. Default: `60s` |
|`PODGC_CACHE_CLEANUP_INTERVAL`    | The maximum amount of time before the cache saving the faulty pods is cleaned up. It is an unsigned sequence of decimal numbers, each with optional fraction and a unit suffix, such as `300ms`, `1.5h` or `2h45m`. Valid time units are `ns`, `us` (or `µs`), `ms`, `s`, `m`, `h`. Minimum: `15m`. Default: `30m`|
|`PODGC_CACHE_EXPIRATION`          | The maximum amount of time before a faulty pod expired from the cache. It is an unsigned sequence of decimal numbers, each with optional fraction and a unit suffix, such as `300ms`, `1.5h` or `2h45m`. Valid time units are `ns`, `us` (or `µs`), `ms`, `s`, `m`, `h`. Minimum: `30m`. Default: `1.5h`|
|`PODGC_KUBERNETES_REQUEST_TIMEOUT`| The maximum amount of time a Kubernetes API request can take. It is an unsigned sequence of decimal numbers, each with optional fraction and a unit suffix, such as `300ms`, `1.5h` or `2h45m`. Valid time units are `ns`, `us` (or `µs`), `ms`, `s`, `m`, `h`. Minimum: `5s`. Default: `30s` |
|`PODGC_KUBERNETES_NAMESPACES`     | List of the namespaces to search for Kubernetes pods. Multiple namespaces are comma-separated `,`. Default: `default`                                                    |
|`PODGC_KUBERNETES_ANNOTATION`     | Annotation to consider when looking for the ttl setting. Default: `podgc-ttl`                                                                                            |
|`PODGC_KUBERNETES_REQUEST_LIMIT`  | Limit the number of pods to retrieve per API request when getting existing pod. Minimum: `100`. Default: `500`                                                           |

### Goals coverage

- [Least privileged should only have list/delete on the `pods` resource](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L35-58)
- [Audit log of what was deleted and why](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L96-121)
- The user decides which pods are deleted:
  - [Namespaces verification](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/kubernetes.go#L132-142)
  - [Annotations verification](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L97)
- [CLI tool that can run inside a Cluster or users laptop](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cmd/podgc/commands/run.go#L24-51)
- [Support supported versions of Kubernetes](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/resources_manager.go#L18-28)
